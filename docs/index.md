### Course "Cloud based processing of geo and Earth observation data"

Course held in Sep 2019, GeoSTAT 2019 in Münster.

- material at [https://neteler.gitlab.io/actinia-introduction/](https://neteler.gitlab.io/actinia-introduction/)

### Course "Analysing environmental data with GRASS GIS"

Course held in Sep 2019, GeoSTAT 2019 in Münster.

- material at [https://neteler.gitlab.io/grass-gis-analysis/](https://neteler.gitlab.io/grass-gis-analysis/)

### Course "Analysing environmental data with GRASS GIS"

Course held in Aug 2018, GeoSTAT 2018 in Prague.

- material at [https://neteler.gitlab.io/grass-gis-analysis/](https://neteler.gitlab.io/grass-gis-analysis/)

### Course "Geodatenverarbeitung mit GRASS GIS"

Workshop held at FOSSGIS 2018, Bonn, 21-23 Mar 2018.

- material at [https://apps.mundialis.de/workshops/bonn2018/](https://apps.mundialis.de/workshops/bonn2018/)

### Course "Unleash the power of GRASS GIS 7"
  
Course held in Aug 2016, FOSS4G 2016 in Bonn.

- material at [https://apps.mundialis.de/workshops/grass_gis_foss4g2016/](https://apps.mundialis.de/workshops/grass_gis_foss4g2016/)

### Previous courses

Please refer to [https://neteler.org/workshops/](https://neteler.org/workshops/)

#### About the trainer

Markus Neteler is partner and general manager at [mundialis](https://www.mundialis.de) GmbH & Co. KG, Bonn, Germany. From 2001-2015 he worked as a researcher in Italy. Markus is co-founder of OSGeo and, since 1998 coordinator of the GRASS GIS development (for details, see his private [homepage](https://neteler.org/)).


([gitlab repository](https://gitlab.com/neteler/neteler.gitlab.io/) of this landing page)
